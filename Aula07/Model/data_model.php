<?php 

include_once 'db_manager.php';

$m = getPosts();

$n = [
    [
    'https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(131).jpg',
    'pink-text',
    'Adventure',
    'This is title of the news',
    'Billy Forester',
    '15/07/2021',
    'Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime
    placeat facere possimus voluptas.',
    ],
    [
      'https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(132).jpg',
      'red-text',
      'AutoMar',
      'Titulo loko de notícia',
      'Billy Nelsom',
      '15/12/2099',
      'Lore ypsum cosmos not , cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime
      placeat facere possimus voluptas.',
    ],
    [
      'https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(133).jpg',
      'blue-text',
      'Blastóise',
      'Notícia velha porém Fresca',
      'Toninha da Vila',
      '19/09/2029',
      'Text qualquer para diferenciar nobis est eligendi optio cumque nihil impedit quo minus id quod maxime
      placeat facere possimus voluptas.',
      ]
  ];