<footer class="page-footer pt-4 mt-4   text-center text-md-left">

    <!--Footer Links-->
    <div class="container">
      <div class="row">

        <!--First column-->
        <div class="col-md-3">
          <h5 class="text-uppercase font-weight-bold">About author </h5>
          <p>Here you can use rows and columns here to organize your footer content.</p>
        </div>
        <!--/.First column-->

        <hr class="w-100 clearfix d-md-none">

        <!--Second column-->
        <div class="col-md-2 ml-auto">
          <h5 class="text-uppercase font-weight-bold">Links</h5>
          <ul class="list-unstyled">
            <li><a href="#!">Link 1</a></li>
            <li><a href="#!">Link 2</a></li>
            <li><a href="#!">Link 3</a></li>
            <li><a href="#!">Link 4</a></li>
          </ul>
        </div>
        <!--/.Second column-->

        <hr class="w-100 clearfix d-md-none">

        <!--Third column-->
        <div class="col-md-2 ml-auto">
          <h5 class="text-uppercase font-weight-bold">Links</h5>
          <ul class="list-unstyled">
            <li><a href="#!">Link 1</a></li>
            <li><a href="#!">Link 2</a></li>
            <li><a href="#!">Link 3</a></li>
            <li><a href="#!">Link 4</a></li>
          </ul>
        </div>
        <!--/.Third column-->

        <hr class="w-100 clearfix d-md-none">

        <!--Fourth column-->
        <div class="col-md-2 ml-auto">
          <h5 class="text-uppercase font-weight-bold">Links</h5>
          <ul class="list-unstyled">
            <li><a href="#!">Link 1</a></li>
            <li><a href="#!">Link 2</a></li>
            <li><a href="#!">Link 3</a></li>
            <li><a href="#!">Link 4</a></li>
          </ul>
        </div>
        <!--/.Fourth column-->

      </div>
    </div>
    <!--/.Footer Links-->

    <hr>

    <!--Social buttons-->
    <div class="text-center mb-3">

      <a class="btn-floating btn-fb"><i class="fab fa-facebook-f"> </i></a>
      <a class="btn-floating btn-tw"><i class="fab fa-twitter"> </i></a>
      <a class="btn-floating btn-gplus"><i class="fab fa-google-plus-g"> </i></a>
      <a class="btn-floating btn-li"><i class="fab fa-linkedin-in"> </i></a>
      <a class="btn-floating btn-dribbble"><i class="fab fa-dribbble"> </i></a>

    </div>
    <!--/.Social buttons-->

    <!--Copyright-->
    <div class="footer-copyright py-3 text-center">
      <div class="container-fluid">
        © 2015 Copyright: <a href="https://mdbootstrap.com/docs/jquery/"> MDBootstrap.com </a>

      </div>
    </div>
    <!--/.Copyright-->

  </footer>
  <!--/.Footer-->


  <!--  SCRIPTS  -->
  <!-- JQuery -->
  <script type="text/javascript" src="../../mdb/js/jquery-3.4.1.min.js"></script>
  <!-- Bootstrap tooltips -->
  <script type="text/javascript" src="../../mdb/js/popper.min.js"></script>
  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" src="../../mdb/js/bootstrap.min.js"></script>
  <!-- MDB core JavaScript -->
  <script type="text/javascript" src="../../mdb/js/mdb.min.js"></script>
  <script>
    new WOW().init();

  </script>
</body>

</html>