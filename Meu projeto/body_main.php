<main>

<div class="container">

  <!--Section: Team v.1-->
  <section class="text-center team-section">

    <!--Grid row-->
    <div class="row text-center">

      <!--Grid column-->
      <div class="col-md-12 mb-4" style="margin-top: -100px;">

        <div class="avatar mx-auto">
          <img src="mdb/img/Solomon.jpg" class="img-fluid rounded-circle z-depth-1"
            alt="First sample avatar image">
        </div>
        <h3 class="my-3 font-weight-bold">
          <strong>Lucas Morais de Oliveira</strong>
        </h3>
        <h6 class="font-weight-bold teal-text mb-4">Desenvolvedor</h6>

        <!--Facebook-->
        <a class="p-2 m-2 fa-lg fb-ic">
          <i class="fab fa-facebook-f grey-text"> </i>
        </a>
        <!--Twitter-->
        <a class="p-2 m-2 fa-lg tw-ic">
          <i class="fab fa-twitter grey-text"> </i>
        </a>
        <!--Instagram-->
        <a class="p-2 m-2 fa-lg ins-ic">
          <i class="fab fa-instagram grey-text"> </i>
        </a>

        <p class="mt-5">Este portifólio se refere a uma atividade
            para a matéria de LPD1D2, Linguagens de Programação.
            Estamos aprendendo PHP e usando algumas ferramentas
            de apoio para concretizar esse conhecimento.</p>

      </div>
      <!--Grid column-->

    </div>
    <!--Grid row-->

  </section>

    <!-- Tab panels -->
    <div class="tab-content">

      <!--Panel 1-->
      <div class="tab-pane fade  show active" id="panel11" role="tabpanel">
        <br>

        <!--Grid row-->
        <div class="row">

          <!--Grid column-->
          <div class="col-md-12">

            <!--Projects section v.4-->
            <section class="text-center mb-5">

              <!--Grid row-->
              <div class="row mb-4">

                <!--Grid column-->
                <div class="col-md-6 mb-4">
                  <div class="card card-image" style="background-image: url('https://mdbootstrap.com/img/Photos/Horizontal/Work/6-col/img%20(41).jpg');">

                    <!-- Content -->
                    <div class="text-white text-center d-flex align-items-center rgba-blue-strong py-5 px-4">
                      <div>
                        <h3 class="mb-4 mt-4 font-weight-bold">
                          <strong>Primeira Atividade</strong>
                        </h3>
                        <p>Na primeira atividade tentamos usar um webhost gratuito
                            mas devido a alguns limites, foi melhor voltarmos
                            para o php puro para aprender.</p>
                        <a class="btn btn-outline-white btn-sm">
                          <i class="fas fa-clone left"></i> View project</a>
                      </div>
                    </div>
                  </div>
                </div>
                <!--Grid column-->

                <!--Grid column-->
                <div class="col-md-6 mb-4">
                  <div class="card card-image" style="background-image: url('https://mdbootstrap.com/img/Photos/Horizontal/Work/6-col/img%20(14).jpg');">

                    <!-- Content -->
                    <div class="text-white text-center d-flex align-items-center rgba-teal-strong py-5 px-4">
                      <div>
                        <h3 class="mb-4 mt-4 font-weight-bold">
                          <strong>Atividade 2</strong>
                        </h3>
                        <p>Na atividade dois eu criei essa página
                            enxugando 600 linhas de código para 5 
                            de php.</p>
                        <a class="btn btn-outline-white btn-sm">
                          <i class="fas fa-clone left"></i> View project</a>
                      </div>
                    </div>
                  </div>
                </div>
                <!--Grid column-->

              </div>
              <!--Grid row-->

              <!--Grid row-->
              <div class="row">

                <!--Grid column-->
                <div class="col-md-6 mb-4">
                  <div class="card card-image" style="background-image: url('https://mdbootstrap.com/img/Photos/Horizontal/Work/6-col/img%20(11).jpg');">

                    <!-- Content -->
                    <div class="text-white text-center d-flex align-items-center rgba-green-strong py-5 px-4">
                      <div>
                        <h3 class="mb-4 mt-4 font-weight-bold">
                          <strong>Atividades Futuras</strong>
                        </h3>
                        <p>Com este espaço, registrarei quais serão as próximas
                            atividades que terei de realizar
                            Estou empolgado para aprender mais sobre o PHP.</p>
                        <a class="btn btn-outline-white btn-sm">
                          <i class="fas fa-clone left"></i> View project</a>
                      </div>
                    </div>
                  </div>
                </div>
                <!--Grid column-->

                <!--Grid column-->
                <div class="col-md-6 mb-4">
                  <div class="card card-image" style="background-image: url('https://mdbootstrap.com/img/Photos/Horizontal/Work/6-col/img%20(42).jpg');">

                    <!-- Content -->
                    <div class="text-white text-center d-flex align-items-center rgba-stylish-strong py-5 px-4">
                      <div>
                        <h3 class="mb-4 mt-4 font-weight-bold">
                          <strong>Projetos futuros</strong>
                        </h3>
                        <p>A minha expectativa para o fim do curso é aprender a criar páginas
                           dinâmicas de uma forma inteligente respeitando as boas
                           práticas e utilizando o php com um certo nível de domínio.
                            </p>
                        <a class="btn btn-outline-white btn-sm">
                          <i class="fas fa-clone left"></i> View project</a>
                      </div>
                    </div>
                  </div>
                </div>
                <!--Grid column-->

              </div>
              <!--Grid row-->

            </section>
            <!--Projects section v.4-->

          </div>
          <!--Grid column-->

        </div>
        <!--Grid row-->

      </div>
      <!--/.Panel 1-->


</div>

</main>
<!--Main Layout-->