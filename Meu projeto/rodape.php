 <!--Footer-->
 <footer class="page-footer mdb-color lighten-3 text-center text-md-left">

<!--Footer Links-->
<div class="container">

  <!--First row-->
  <div class="row">
    <div class="col-md-12">

      <h5 class="my-5 d-flex justify-content-center">Se você precisar me mandar alguma sugestão, entre em contato:
        l.morais@aluno.ifsp.edu.br
      </h5>
    </div>
  </div>
  <!--/First row-->
</div>
<!--/Footer Links-->

<!--Copyright-->
<div class="footer-copyright text-center py-3 wow fadeIn" data-wow-delay="0.3s">
  <div class="container-fluid">
    &copy; 2019 Copyright:
    <a href="https://mdbootstrap.com/docs/jquery/"> IFSP- Guarulhos 2021 </a>
  </div>
</div>
<!--/Copyright-->

</footer>
<!--/Footer-->