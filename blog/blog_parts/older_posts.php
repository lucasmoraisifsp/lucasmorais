<hr class="mb-5">

<!--Section: Blog v.2-->
<section class="section extra-margins text-center pb-3 wow fadeIn" data-wow-delay="0.3s">

  <!--Section heading-->
  <h2 class="font-weight-bold text-center h1 my-5">Older posts</h2>
  <!--Section description-->
  <p class="text-center grey-text mb-5 mx-auto w-responsive">Duis aute irure dolor in reprehenderit in voluptate
    velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
    culpa qui officia deserunt mollit anim id est laborum.</p>

  <!--Grid row-->
  <div class="row">

    <!--Grid column-->
    <div class="col-lg-4 col-md-12 mb-4">
      <!--Featured image-->
      <div class="view overlay z-depth-1 mb-2">
        <img src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(131).jpg" class="img-fluid"
          alt="First sample image">
        <a>
          <div class="mask rgba-white-slight"></div>
        </a>
      </div>

      <!--Excerpt-->
      <a href="" class="pink-text">
        <h6 class="mb-3 mt-3"><i class="fas fa-map "></i><strong> Adventure</strong></h6>
      </a>
      <h4 class="mb-3 font-weight-bold">This is title of the news</h4>
      <p>by <a><strong>Billy Forester</strong></a>, 15/07/2016</p>
      <p>Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime
        placeat facere possimus voluptas.</p>
      <a class="btn btn-primary">Read more</a>
    </div>
    <!--Grid column-->

    <!--Grid column-->
    <div class="col-lg-4 col-md-6 mb-4">
      <!--Featured image-->
      <div class="view overlay z-depth-1 mb-2">
        <img src="https://mdbootstrap.com/img/Photos/Others/img6.jpg" class="img-fluid" alt="Second sample image">
        <a>
          <div class="mask rgba-white-slight"></div>
        </a>
      </div>

      <!--Excerpt-->
      <a href="" class="indigo-text">
        <h6 class="mb-3 mt-3"><i class="fas fa-graduation-cap"></i><strong> Education</strong></h6>
      </a>
      <h4 class="mb-3 font-weight-bold">This is title of the news</h4>
      <p>by <a><strong>Billy Forester</strong></a>, 12/07/2016</p>
      <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum
        deleniti atque corrupti quos dolores.</p>
      <a class="btn btn-primary">Read more</a>
    </div>
    <!--Grid column-->

    <!--Grid column-->
    <div class="col-lg-4 col-md-6 mb-4">
      <!--Featured image-->
      <div class="view overlay z-depth-1 mb-2">
        <img src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(33).jpg" class="img-fluid"
          alt="Thrid sample image">
        <a>
          <div class="mask rgba-white-slight"></div>
        </a>
      </div>

      <!--Excerpt-->
      <a href="" class="cyan-text">
        <h6 class="mb-3 mt-3"><i class="fas fa-fire "></i><strong> Culture</strong></h6>
      </a>
      <h4 class="mb-3 font-weight-bold">This is title of the news</h4>
      <p>by <a><strong>Billy Forester</strong></a>, 10/07/2016</p>
      <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, quia consequuntur magni
        dolores eos qui ratione voluptatem.</p>
      <a class="btn btn-primary">Read more</a>
    </div>
    <!--Grid column-->

  </div>
  <!--Grid row-->

</section>
<!--Section: Blog v.2-->