 <!--Section: Blog v.3-->
 <section class="section extra-margins pb-3 text-center text-lg-left wow fadeIn" data-wow-delay="0.3s">

<!--Section heading-->
<h2 class="font-weight-bold text-center h1 my-5">Recent posts</h2>
<!--Section description-->
<p class="text-center grey-text mb-5 mx-auto w-responsive">Duis aute irure dolor in reprehenderit in voluptate
  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
  culpa qui officia deserunt mollit anim id est laborum.</p>

  <?php 
    $row = [
        'https://mdbootstrap.com/img/Photos/Others/img (38).jpg',
        'teal-text',
        'Lifestyle',
        'Notícia Urgente e novinha!!!',
        'Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime
         placeat facere possimus, omnis voluptas assumenda est, omnis dolor.',
        'Lucas Salomão',
        '19/09/2020'
    ];
    include 'post_row.php';
    $row = [
        'https://mdbootstrap.com/img/Photos/Others/img (32).jpg',
        'blue-text',
        'Qualquer',
        'Notícia Urgente e azul!!!',
        ' Lorena ipsa Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime
        placeat facere possimus, omnis voluptas assumenda est, omnis dolor.',
        'Lucas Salomão',
        '19/09/2021'
    ];
    include 'post_row.php';
  ?>